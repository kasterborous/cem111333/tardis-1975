-- 1975 TARDIS (classic doors)

local door_offset = 26.70 --26.70

local T={}
T.Base="baker_1975"
T.Name="1975 TARDIS"
T.ID="baker_1975_cl"
T.EnableClassicDoors = true

T.IsVersionOf = "baker_1975"

T.Interior={
	Portal = {
		pos = Vector(-1.1,-150,38.8),
		ang = Angle(0, 90, 0),
		width = 150,
		height = 200
	},
	Fallback = {
		pos = Vector(0.382, -127.657, 22.653),
		ang = Angle(0, 90, 0),
	},
	Sounds={
		Door={
			enabled=true,
			open = "doctorwho1200/hartnell/door_fast.wav",
			close = "doctorwho1200/hartnell/door_fast.wav",
		},
	},
	Parts={
		baker_1975_intdoors=false,
		intdoor = {
			model="models/cem111333/baker/bigdoor.mdl",
			pos = Vector(-29.38, -146.26, 67.635),		ang = Angle(0, -90, 0),
		},
		door = {
			posoffset = Vector(door_offset, 1.25, -44.75),
		},
	},
	Controls = {
		baker_1975_handlelever_5b = "door",
		baker_1975_handlelever_1 = "door",
	},
	IntDoorAnimationTime = 4.5,
}

T.Exterior = {
	Portal = {
		pos = Vector(25.5, 1, 44.75),
		ang = Angle(0, 0, 0),
		width = 40,
		height = 85
	},
	Parts = {
		door = {
			posoffset = Vector(-door_offset, -1.25, -44.75),
		},
	},
}

TARDIS:AddInterior(T)