local T={}
T.Base="base"
T.Name="1975 TARDIS"
T.ID="baker_1975"

T.Versions = {
	randomize = true,
	allow_custom = true,
	randomize_custom = true,

	main = {
		classic_doors_id = "baker_1975_cl",
		double_doors_id = "baker_1975",
	},
	other = {},
}

T.Interior={
	Model="models/cem111333/baker/interior.mdl",

	Portal={
		pos=Vector(-1.5,-208,38.8),
		ang=Angle(0,90,0),
		width=300,
		height=190
	},
	Scanners = {
        {
            mat = "models/karmal/75/console_screen",
            part = "baker_1975_consolescreen",
			width = 2000,
            height = 2000,
            ang = Angle(0, 0, 0),
            fov = 90,
        }
    },
	Fallback={
		pos=Vector(-1.348, -177.07, 4.814),
		ang=Angle(0,0,0)
	},
	ExitDistance=2200,
	Light={
		color=Color(170,164,150),
		warncolor=Color(213, 53, 40),
		pos=Vector(0.071, 10.513, 100.606),
		brightness=1
	},
	Lights = {
		{
			color = Color(255, 217, 165),
			pos = Vector(0.071, 25.513, 150.606),
			brightness = 0.021,
			nopower = true,
			warncolor = Color(60, 0, 0),
		},
	},
	LightOverride = {
		basebrightness = 0.16,
		nopowerbrightness = 0.001
	},
	IdleSound={
		{
			path="cem111333/baker/interior_static.wav",
			volume=1
		}
	},
	ScreensEnabled = false,
	Screens = {
		{
			pos = Vector(18.7, 19.6,42.74),
			ang = Angle(0, 150, 19.5),
			width = 205,
			height = 155,
			visgui_rows = 4,
			power_off_black = false
		}
	},
	Screens = false,
	Parts={
		console=false,
		door={
			model="models/cem111333/baker/doors.mdl",
			posoffset=Vector(27.20, 0, -44.75),
			angoffset=Angle(0,90,0)
		},
		baker_1975_rotor 		= {pos = Vector(0, 0, 3)},
		baker_1975_console 		= {},
		baker_1975_consoleplastic	= {},
		baker_1975_panelboxes 		= {},
		baker_1975_lightbox			= {},
		baker_1975_roundels 		= {},
		baker_1975_sofit			= {},
		baker_1975_intdoors 		= {pos = Vector(-29.38, -146.26, 67.635),		ang = Angle(0, -90, 0)},

		baker_1975_consolescreen		= {},

		baker_1975_consolelamps 		= {},

		baker_1975_colorscopes 		= {},

		baker_1975_colorstrip		= {},

		baker_1975_smalllights_1		= {},
		baker_1975_smalllights_1a	= {},
		baker_1975_smalllights_1b	= {},
		baker_1975_smalllights_1c	= {},
		baker_1975_smalllights_3		= {},
		baker_1975_smalllights_4		= {},

		baker_1975_biglever_1		= {pos = Vector(34.8008, -0.125371, 39.603),		ang = Angle(-19.65, 180, 0)},
		baker_1975_biglever_2		= {pos = Vector(18.4897, -28.9761, 39.9429),		ang = Angle(-19.65, 120, 0)},
		baker_1975_biglever_3		= {pos = Vector(15.7184, -30.5761, 39.9429),		ang = Angle(-19.65, 120, 0)},

		baker_1975_bigredbutton		= {pos = Vector(33.5564, 6.8587, 40.257),		ang = Angle(-19.65, 180, 0)},

		baker_1975_wheelknob_1		= {pos = Vector(-30.41, -9.6988, 41.5483),		ang = Angle(-19.65, 0, 0)},
		baker_1975_wheelknob_2		= {pos = Vector(-23.3212, 23.0445, 40.9203),		ang = Angle(-19.65, -60, 0)},
		baker_1975_wheelknob_3		= {pos = Vector(-8.16573, 31.7945, 40.9203),		ang = Angle(-19.65, -60, 0)},

		baker_1975_knob_2		= {pos = Vector(-15.0205, 26.1673, 41.457),		ang = Angle(-19.65, -60, 0)},
		baker_1975_knob_4		= {pos = Vector(34.0297, -8.08344, 40.0955),		ang = Angle(-19.65, 180, 0)},
		baker_1975_knob_6		= {pos = Vector(-12.264, -35.9404, 38.8115),		ang = Angle(-19.65, 60, 0)},

		baker_1975_largeknob		= {pos = Vector(-36.4811, -14.6988, 39.2213),		ang = Angle(-19.65, 0, 0)},

		baker_1975_joystick		= {pos = Vector(10.1508, 34.8189, 39.6436),		ang = Angle(-19.65, -120, 0)},

		baker_1975_handlelever_1		= {pos = Vector(-35.7747, -3.1988, 39.4735),		ang = Angle(19.65, 180, 0)},
		baker_1975_handlelever_5a	= {pos = Vector(26.8473, -26.652, 39.1695),		ang = Angle(19.65, -60, 0)},
		baker_1975_handlelever_5b	= {pos = Vector(9.52679, -36.652, 39.1695),		ang = Angle(19.65, -60, 0)},
		baker_1975_handlelever_6		= {pos = Vector(-31.553, -24.2499, 38.9802),		ang = Angle(19.65, -120, 0)},

		baker_1975_pegbutton_1a		= {pos = Vector(-34.1468, 14.7705, 40.0017),		ang = Angle(-19.65, 0, 0)},
		baker_1975_pegbutton_1b		= {pos = Vector(-35.7949, 16.0205, 39.4132),		ang = Angle(-19.65, 0, 0)},
		baker_1975_pegbutton_1c		= {pos = Vector(-35.7949, 14.7705, 39.4132),		ang = Angle(-19.65, 0, 0)},
		baker_1975_pegbutton_5a		= {pos = Vector(12.2115, -27.2985, 41.5825),		ang = Angle(-19.65, 120, 0)},
		baker_1975_pegbutton_5b		= {pos = Vector(17.4077, -24.2985, 41.5825),		ang = Angle(-19.65, 120, 0)},

		baker_1975_squarebutton_2a		= {pos = Vector(-12.9884, 35.2477, 39.0074),		ang = Angle(-19.65, -60, 0)},
		baker_1975_squarebutton_2b		= {pos = Vector(-23.9004, 28.9477, 39.0074),		ang = Angle(-19.65, -60, 0)},
		baker_1975_squarebutton_4a		= {pos = Vector(28.4709, -4.52294, 42.0728),		ang = Angle(0, -90, -19.65)},
		baker_1975_squarebutton_4b		= {pos = Vector(28.4709, 4.47706, 42.0728),		ang = Angle(0, 90, 19.65)},
		baker_1975_squarebutton_6		= {pos = Vector(-10.5379, -38.3507, 38.3744),		ang = Angle(19.65, -120, 0)},

		baker_1975_switch4a		= {pos = Vector(28.614, -5.75035, 42.033),		ang = Angle(-19.65, 180, 0)},
		baker_1975_switch4b		= {pos = Vector(28.614, 5.74965, 42.033),		ang = Angle(-19.65, 180, 0)},
		baker_1975_switch4c		= {pos = Vector(30.9685, -7.75035, 41.1923),		ang = Angle(-19.65, 180, 0)},
		baker_1975_switch4d		= {pos = Vector(30.9685, -5.75035, 41.1923),		ang = Angle(-19.65, 180, 0)},
		baker_1975_switch4e		= {pos = Vector(30.9685, -3.75035, 41.1923),		ang = Angle(-19.65, 180, 0)},
		baker_1975_switch4f		= {pos = Vector(30.9684, 2.99965, 41.1923),		ang = Angle(-19.65, 180, 0)},
		baker_1975_switch6		= {pos = Vector(-9.92577, -37.2904, 38.8115),		ang = Angle(-19.65, 60, 0)},

		baker_1975_redbuttons_1		= {pos = Vector(-8.06485, 26.7798, 42.5014),		ang = Angle(-19.65, -60, 0)},
		baker_1975_redbuttons_2		= {pos = Vector(-19.0287, 20.4498, 42.5014),		ang = Angle(-19.65, -60, 0)},

		baker_1975_keypad		= {},

		baker_1975_redsphere		= {},
		baker_1975_kknob			= {pos = Vector(-21.0539, -20.4649, 42.1339),		ang = Angle(19.65, -120, 0)},

		baker_1975_blackswitches_2	= {},
		baker_1975_blackswitches_3	= {},
		baker_1975_blackswitches_4	= {},
		baker_1975_blackswitches_6	= {},

		baker_1975_sliderswitches_1	= {},
		baker_1975_sliderswitches_2	= {},

		baker_1975_speaker_1		= {pos = Vector(-19.054, 33.1536, 38.5712),		ang = Angle(-19.65, -60, 0)},
		baker_1975_speaker_2		= {pos = Vector(-12.9485, -22.4259, 42.9746),		ang = Angle(-19.65, 60, 0)},
	},
	PartTips = {
		baker_1975_biglever_1 = {pos = Vector(34.406, -0.143, 40.781), right = false, up = true,},
		baker_1975_biglever_2 = {pos = Vector(18.725, -29.314, 41.68), right = true, up = true,},
		baker_1975_colorscopes = {pos = Vector(7.831, -31.654, 45.34), right = true, up = true,},
		baker_1975_biglever_3 = {pos = Vector(15.756, -30.633, 41.463), right = false, up = true,},
		baker_1975_squarebutton_4b = {pos = Vector(29.304, 3.616, 43.523), right = false, up = true,},
		baker_1975_bigredbutton = {pos = Vector(33.805, 6.949, 40.837), right = false, up = true,},
		baker_1975_wheelknob_1 = {pos = Vector(-30.731, -9.567, 41.868), right = true, up = true,},
		baker_1975_wheelknob_2 = {pos = Vector(-23.65, 23.528, 42.789), right = true, up = true,},
		baker_1975_wheelknob_3 = {pos = Vector(-8.32, 32.09, 41.611), right = false, up = true,},
		baker_1975_wheelknob_3 = {pos = Vector(-8.32, 32.09, 41.611), right = false, up = true,},
		baker_1975_knob_2 = {pos = Vector(-15.097, 26.361, 41.221), right = false, up = true,},
		baker_1975_knob_4 = {pos = Vector(34.137, -8.087, 40.156), right = false, up = true,},
		baker_1975_knob_6 = {pos = Vector(-12.486, -36.221, 39.358), right = false, up = true,},
		baker_1975_joystick = {pos = Vector(10.095, 34.801, 39.273), right = false, up = true,},
		baker_1975_handlelever_1 = {pos = Vector(-36.137, -3.293, 39.444), right = false, up = true,},
		baker_1975_handlelever_5a = {pos = Vector(26.872, -26.783, 38.879), right = false, up = true,},
		baker_1975_handlelever_5b = {pos = Vector(9.573, -36.951, 39.077), right = false, up = true,},
		baker_1975_squarebutton_2a = {pos = Vector(26.872, -26.783, 38.879), right = false, up = true,},
		baker_1975_handlelever_6 = {pos = Vector(-31.703, -24.485, 39.063), right = false, up = true,},
		baker_1975_switch6 = {pos = Vector(-9.93, -37.33, 38.891), right = true, up = true,},
		baker_1975_redbuttons_2 = {pos = Vector(-19.622, 21.515, 42.056), right = false, up = true,},
		baker_1975_keypad = {pos = Vector(-36.433, -9.725, 38.892), right = false, up = true,},
		baker_1975_kknob = {pos = Vector(-21.285, -20.768, 43.302), right = false, up = true,},
		baker_1975_largeknob = {pos = Vector(-36.961, -14.721, 39.953), right = true, up = true,},
		baker_1975_speaker_2 = {pos = Vector(-12.646, -22.187, 42.76), right = false, up = true,},
		baker_1975_pegbutton_1b = {pos = Vector(-35.939, 16.063, 39.572), right = false, up = true,},
		baker_1975_sliderswitches_1 = {pos = Vector(-31.389, -2.271, 41.856), right = false, up = true,},
	},
	CustomTips = {
		{ text = "Manual Flight Control", pos = Vector(22.454, -20.007, 41.264), right = true, up = true,},
	},
	Controls = {

		baker_1975_handlelever_1	= "baker_1975_intdoors",
		baker_1975_handlelever_5a	= "doorlock",
		baker_1975_handlelever_5b	= "door",
		baker_1975_squarebutton_4b	= "vortex_flight",
		baker_1975_biglever_3		= "teleport",
		baker_1975_pegbutton_1b		= "teleport",
		baker_1975_biglever_2		= "physlock",
		baker_1975_wheelknob_2       = "toggle_scanners",
		baker_1975_bigredbutton		= "power",
		baker_1975_colorscopes		= "thirdperson",
		baker_1975_wheelknob_1		= "fastreturn",
		baker_1975_keypad			= "coordinates",
		baker_1975_kknob			= "cloak",
		baker_1975_largeknob		= "random_coords",
		baker_1975_speaker_2		= "music",
		baker_1975_knob_6			= "repair",
		baker_1975_switch6			= "float",
		baker_1975_handlelever_6	= "hads",
		baker_1975_redbuttons_2		= "destination",
		baker_1975_knob_2			= "spin_cycle",
		baker_1975_wheelknob_3		= "redecorate",
		baker_1975_joystick			= "toggle_screens",
		baker_1975_knob_4			= "handbrake",
		baker_1975_biglever_1		= "flight",
		baker_1975_sliderswitches_1  = "virtualconsole"

	},
	Sounds={
		Teleport={
			demat="liam.T/baker/demat.wav",
			mat="liam.T/baker/mat.wav",
			fullflight = "liam.T/baker/full.wav",
		},
		Power={
			On="liam.T/baker/powerup.wav", -- Power On
			Off="liam.T/baker/powerdown.wav" -- Power Off
		},
		FlightLoop="liam.T/baker/interior_flight.wav",
		RepairFinish="cem111333/baker/repairfinish.wav",
		Door={
			enabled=true,
			open="cem111333/baker/dooropen.wav",
			close="cem111333/baker/doorclose.wav",
		},
		Lock="cem111333/baker/lock.wav",
		Cloak = "cem111333/baker/cloak.wav",
		CloakOff = "cem111333/baker/uncloak.wav",
	},
	TipSettings = {
		view_range_min = 40,
		view_range_max = 55,
		style = "baker_1975",
	},

}
T.Exterior={
	Model="models/cem111333/baker/exterior.mdl",
	Mass=2900,
	Portal={
		pos=Vector(26, -0.15, 44.75),
		ang=Angle(0,0,0),
		width=48,
		height=85
	},
	Fallback={
		pos=Vector(35,0,5),
		ang=Angle(0,0,0)
	},
	Light={
		enabled=true,
		pos=Vector(0.5, 2, 107),
		color=Color(255,228,91)
	},
	Sounds={
		Teleport={
			demat="liam.T/baker/demat.wav",
			mat="liam.T/baker/mat.wav",
			fullflight = "liam.T/baker/full.wav",
		},
		FlightLoop="liam.T/baker/flight_loop.wav",
		RepairFinish="cem111333/baker/repairfinish.wav",
		Door={
			enabled=true,
			open="cem111333/baker/dooropen.wav",
			close="cem111333/baker/doorclose.wav",
		},
		Lock="cem111333/baker/lock.wav",
		Cloak = "cem111333/baker/cloak.wav",
		CloakOff = "cem111333/baker/uncloak.wav",
	},

	Parts={
		door={
			model="models/cem111333/baker/doorsext.mdl",
			posoffset=Vector(-27.20, 0, -44.75),
			angoffset=Angle(0,-90,0)
		},
		vortex={
			model="models/doctorwho1200/baker/1974timevortex.mdl",
			pos=Vector(0,0,0),
			ang=Angle(0,180,0),
			scale=10
		}
	},
	Teleport={
		SequenceSpeed=0.63,
		SequenceSpeedFast=1, -- How fast the TARDIS will go through these values - note the sound file doesn't automatically allign with this.
		DematSequence={
			150,
			200,
			100,
			150,
			50,
			100,
			0
		},
		MatSequence={
			100,
			50,
			150,
			100,
			200,
			150,
			255
		}
	},
}

T.Interior.TextureSets = {
	["screenon"] = {
		prefix = "models/karmal/75/",
		{ "baker_1975_consolescreen", 1, "console_screen" },
	},
	["screenvortex"] = {
		prefix = "models/doctorwho1200/baker/",
		{ "baker_1975_consolescreen", 2, "1974vortex2" },
	},
	["screenoff"] = {
		prefix = "models/karmal/75/",
		{ "baker_1975_consolescreen", 1, "console_screen" },
	},
	["screenwarning"] = {
		prefix = "models/karmal/75/",
		{ "baker_1975_consolescreen", 1, "console_screen" },
	},
}

T.Exterior.CustomHooks = {
	screen_textures = {
		{
			["PowerToggled"] = true,
			["HealthWarningToggled"] = true,
			["StopDemat"] = true,
			["MatStart"] = true,
		},
		function(self)
			local power = self:GetData("power-state")
			local warning = self:GetData("health-warning", false)
			local vortex = self:GetData("vortex")

			if not power then
				self.interior:ApplyTextureSet("screenoff")
			elseif warning then
				self.interior:ApplyTextureSet("screenwarning")
			elseif vortex then
				self.interior:ApplyTextureSet("screenvortex")
			else
				self.interior:ApplyTextureSet("screenon")
			end
		end,
	},
}

TARDIS:AddInterior(T)