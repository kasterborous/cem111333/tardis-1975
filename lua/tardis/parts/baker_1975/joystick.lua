local PART={}
PART.ID = "baker_1975_joystick"
PART.Name = "Joystick"
PART.Model = "models/karmal/75/joystick.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "karmal/75/joystick.wav"

TARDIS:AddPart(PART)