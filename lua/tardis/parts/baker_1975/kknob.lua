local PART={}
PART.ID = "baker_1975_kknob"
PART.Name = "Large_Knob"
PART.Model = "models/karmal/75/k_knob.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "karmal/75/k_knob.wav"

TARDIS:AddPart(PART)