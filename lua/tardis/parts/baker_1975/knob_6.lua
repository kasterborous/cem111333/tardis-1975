local PART={}
PART.ID = "baker_1975_knob_6"
PART.Name = "Knob_6"
PART.Model = "models/karmal/75/knob_red.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "cem111333/baker/button.wav"

TARDIS:AddPart(PART)