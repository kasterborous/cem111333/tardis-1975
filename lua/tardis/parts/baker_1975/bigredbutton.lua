local PART={}
PART.ID = "baker_1975_bigredbutton"
PART.Name = "Big_Red_Button"
PART.Model = "models/karmal/75/big_red_button.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "karmal/75/big_red_button.wav"

TARDIS:AddPart(PART)