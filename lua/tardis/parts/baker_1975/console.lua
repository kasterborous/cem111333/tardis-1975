local PART={}
PART.ID = "baker_1975_console"
PART.Name = "Console"
PART.Model = "models/karmal/75/console.mdl"
PART.AutoSetup = true
PART.Collision = true

if SERVER then
	function PART:Use(ply)
		if ply:IsPlayer() and (not ply:GetTardisData("thirdperson")) and CurTime()>ply:GetTardisData("outsidecool", 0) then
			TARDIS:Control("thirdperson_careful", ply)
		end
	end
	function PART:Think()
		local exterior = self.exterior
		local rotor = self.interior:GetPart("baker_1975_rotor")
		local power = self.exterior:GetData("power-state")

		if power == true then
			self:SetSubMaterial(4, "models/karmal/75/console_panels_metal")
			self:SetSubMaterial(3, "models/karmal/75/console_panels_metal_2")
			self:SetSubMaterial(5, "models/karmal/75/console_panels_metal_3")
			--rotor:SetSubMaterial(9 , "models/karmal/75/rotor_core_lamps_bulb")
		else
			self:SetSubMaterial(4, "models/karmal/75/console_panels_metal_off")
			self:SetSubMaterial(3, "models/karmal/75/console_panels_metal_2_off")
			self:SetSubMaterial(5, "models/karmal/75/console_panels_metal_3_off")
			--rotor:SetSubMaterial(9 , "models/karmal/75/rotor_core_lamps_bulb_off")
		end
	end
end

TARDIS:AddPart(PART)