local PART={}
PART.ID = "baker_1975_largeknob"
PART.Name = "Large_Knob"
PART.Model = "models/karmal/75/large_knob.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "karmal/75/knob_spin.wav"

TARDIS:AddPart(PART)