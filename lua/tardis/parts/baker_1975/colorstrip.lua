local PART={}
PART.ID = "baker_1975_colorstrip"
PART.Name = "Color_Strip"
PART.Model = "models/karmal/75/color_strip.mdl"
PART.AutoSetup = true
PART.Collision = true

if CLIENT then
	function PART:Think()
		local exterior=self.exterior
			if exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex") or exterior:GetData("float") then
			self:SetSubMaterial(1 , "models/karmal/75/color_strip_flight")
		else
			self:SetSubMaterial(1 , "models/karmal/75/color_strip")
		end
	end
end

TARDIS:AddPart(PART)