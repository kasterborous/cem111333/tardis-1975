local PART={}
PART.ID = "baker_1975_knob_2"
PART.Name = "Knob_2"
PART.Model = "models/karmal/75/knob.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "cem111333/baker/button.wav"

TARDIS:AddPart(PART)