local PART={}
PART.ID = "baker_1975_keypad"
PART.Name = "Keypad"
PART.Model = "models/karmal/75/keypad.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "karmal/75/keypad.wav"

TARDIS:AddPart(PART)