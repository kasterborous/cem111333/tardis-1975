local PART={}
PART.ID = "baker_1975_biglever_1"
PART.Name = "Big_Lever_1"
PART.Model = "models/karmal/75/big_lever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "karmal/75/big_lever.wav"

TARDIS:AddPart(PART)