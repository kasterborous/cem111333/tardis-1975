local PART={}
PART.ID = "baker_1975_handlelever_1"
PART.Name = "Handle_Lever_1"
PART.Model = "models/karmal/75/handle_lever_2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "karmal/75/handle_lever.wav"

TARDIS:AddPart(PART)