local PART={}
PART.ID = "baker_1975_smalllights_1"
PART.Name = "Small_Lights_1"
PART.Model = "models/karmal/75/small_lights_1.mdl"
PART.AutoSetup = true
PART.Collision = true

if CLIENT then
	function PART:Think()
		local power=self.exterior:GetData("power-state")
		if power == true then
			self:SetSubMaterial(1 , "models/karmal/75/colored_lights")
		else
			self:SetSubMaterial(1 , "models/karmal/75/colored_lights_off")
		end
	end
end

TARDIS:AddPart(PART)