local PART={}
PART.ID = "baker_1975_switch4c"
PART.Name = "Toggle_Switch_4c"
PART.Model = "models/karmal/75/toggle_switch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "karmal/75/toggle_switch.wav"

TARDIS:AddPart(PART)