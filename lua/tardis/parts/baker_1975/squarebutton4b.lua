local PART={}
PART.ID = "baker_1975_squarebutton_4b"
PART.Name = "Square_Button_4b"
PART.Model = "models/karmal/75/square_button.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "cem111333/baker/switch.wav"

TARDIS:AddPart(PART)