local PART={}
PART.ID = "baker_1975_handlelever_5b"
PART.Name = "Handle_Lever_5b"
PART.Model = "models/karmal/75/handle_lever_3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "karmal/75/handle_lever.wav"

TARDIS:AddPart(PART)