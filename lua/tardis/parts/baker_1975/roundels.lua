local PART={}
PART.ID = "baker_1975_roundels"
PART.Name = "Roundel_Backing"
PART.Model = "models/cem111333/baker/roundel_backing.mdl"
PART.AutoSetup = true
PART.Collision = true

if CLIENT then
	function PART:Think()

    	local exterior = self.exterior
		local power = self.exterior:GetData("power-state")
		mat = Material("models/cem111333/baker/roundel_backing")

		if power == true then
		if exterior:GetData("health-warning") then
		self:SetMaterial("models/cem111333/baker/roundel_backing_warning")
		else
		self:SetMaterial("models/cem111333/baker/roundel_backing")
		end
		if exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex") then
		self:SetMaterial("models/cem111333/baker/roundel_backing_flight")
		else
		self:SetMaterial("models/cem111333/baker/roundel_backing")
		end
		else
		self:SetColor(Color(255,255,255))
		self:SetMaterial("models/cem111333/baker/roundel_backing_off")
		end
	end
end

TARDIS:AddPart(PART)