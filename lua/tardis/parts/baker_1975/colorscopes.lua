local PART={}
PART.ID = "baker_1975_colorscopes"
PART.Name = "Color_Scopes"
PART.Model = "models/karmal/75/color_scopes.mdl"
PART.AutoSetup = true
PART.Collision = true

if CLIENT then
	function PART:Think()
		local exterior=self.exterior
			if exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex") or exterior:GetData("float") then
			self:SetSubMaterial(1 , "models/karmal/75/color_wheel_flight")
		else
			self:SetSubMaterial(1 , "models/karmal/75/color_wheel")
		end
	end
end

TARDIS:AddPart(PART)