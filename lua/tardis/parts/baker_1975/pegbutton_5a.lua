local PART={}
PART.ID = "baker_1975_pegbutton_5a"
PART.Name = "Peg_Button_5a"
PART.Model = "models/karmal/75/peg_button.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "karmal/75/peg_button.wav"

TARDIS:AddPart(PART)