local PART={}
PART.ID = "baker_1975_consolelamps"
PART.Name = "Console_Lamps"
PART.Model = "models/karmal/75/console_lamps.mdl"
PART.AutoSetup = true
PART.Collision = true

if CLIENT then
	function PART:Think()
		local power=self.exterior:GetData("power-state")
		if power == true then
			self:SetSubMaterial(1 , "models/karmal/75/console_lamps")
			self:SetSubMaterial(2 , "models/karmal/75/console_lamps_flash_slow")
			self:SetSubMaterial(3 , "models/karmal/75/console_lamps_flash")
		else
			self:SetSubMaterial(1 , "models/karmal/75/console_lamps_off")
			self:SetSubMaterial(2 , "models/karmal/75/console_lamps_off")
			self:SetSubMaterial(3 , "models/karmal/75/console_lamps_off")
		end
	end
end

TARDIS:AddPart(PART)