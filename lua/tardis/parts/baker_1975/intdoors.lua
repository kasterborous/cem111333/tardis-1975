local PART={}
PART.ID = "baker_1975_intdoors"
PART.Name = "1975 TARDIS Big Doors"
PART.Model = "models/cem111333/baker/bigdoor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.15

PART.Sound = "cem111333/baker/door.wav"
PART.SoundPos = Vector(-1.10, -166.15, 41.05)

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn())
	end

	function PART:Toggle( bEnable, ply )
		sound.Play(self.Sound, self:LocalToWorld(self.SoundPos))
		self:SetOn(bEnable)
		self:SetCollide(not bEnable)
	end
end

TARDIS:AddPart(PART)