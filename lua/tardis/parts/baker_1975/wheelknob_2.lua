local PART={}
PART.ID = "baker_1975_wheelknob_2"
PART.Name = "Wheel_Knob_2"
PART.Model = "models/karmal/75/wheel_knob.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "karmal/75/knob_spin.wav"

TARDIS:AddPart(PART)