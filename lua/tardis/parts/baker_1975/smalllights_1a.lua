local PART={}
PART.ID = "baker_1975_smalllights_1a"
PART.Name = "Small_Lights_1a"
PART.Model = "models/karmal/75/small_lights_1a.mdl"
PART.AutoSetup = true
PART.Collision = true

if CLIENT then
	function PART:Think()
		local power=self.exterior:GetData("power-state")
		local switch = TARDIS:GetPart(self.interior,"baker_1975_pegbutton_1a")
		if power == true then
			if ( switch:GetOn() ) then
				self:SetSubMaterial(1 , "models/karmal/75/colored_lights")
			else
				self:SetSubMaterial(1 , "models/karmal/75/colored_lights_off")
			end
		else
			self:SetSubMaterial(1 , "models/karmal/75/colored_lights_off")
		end
	end
end

TARDIS:AddPart(PART)