local style = {
	style_id = "baker_1975",
	style_name = "1975",
	font = "GModWorldtip",
	padding = 10,
	offset = 30,
	fr_width = 5,
	colors = {
		normal = {
			text = Color(41, 41, 41, 250),
			background = Color(203, 195, 184),
			frame = Color(70, 70, 70, 100),
		},
		highlighted = {
			text = Color(48, 131, 37, 250),
			background = Color(203, 195, 184),
			frame = Color(48, 131, 37, 154),
		}
	}
}
TARDIS:AddTipStyle(style)
