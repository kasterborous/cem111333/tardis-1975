TARDIS:AddControl({
	id = "baker_1975_screen_vortex",
	tip_text = "Vortex scanner",
	serveronly=true,
	power_independent = false,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply)
		local show = not self:GetData("baker_1975_show_vortex", true)
		self:SetData("baker_1975_show_vortex", show, true)
		self:CallHook("baker_1975_screen_update")
	end,
})