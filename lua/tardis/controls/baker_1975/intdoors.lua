TARDIS:AddControl({
    id = "baker_1975_intdoors",
    tip_text = "Main doors",
    serveronly=true,
    power_independent = false,
    screen_button = { virt_console = false, mmenu = false, },

    int_func=function(self,ply)
        local intdoors = TARDIS:GetPart(self, "baker_1975_intdoors")
        if not IsValid(intdoors) then return end
        intdoors:Toggle(!intdoors:GetOn(), ply)
    end,
})